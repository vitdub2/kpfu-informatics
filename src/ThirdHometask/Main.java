package ThirdHometask;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

public class Main {
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "admin";
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/postgres";

    public static void main(String[] args) throws SQLException {
        Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
        UserRepositoryJdbcImpl impl = new UserRepositoryJdbcImpl(connection);
        List<User> users = impl.findAllByAge(19);
        users.stream().forEach(System.out::println);
    }
}
