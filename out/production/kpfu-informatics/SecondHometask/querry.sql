/*1*/
select model, speed, hd from PC where price < 500;

/*2*/
select maker from Product where type = 'Printer' group by maker;

/*3*/
select model, ram, screen from Laptop where price > 1000;

/*4*/
select * from Printer where color = 'y';

/*5*/
select model, speed, hd from PC where price < 600 and (cd = '12x' or cd = '24x');

/*6*/
select maker, speed from Product inner join Laptop on Product.model = Laptop.model where hd >= 10;

/*7*/
select distinct Product.model, price from PC inner join Product on Product.model = PC.model where maker = 'B'
union
select distinct Product.model, price from Laptop inner join Product on Product.model = Laptop.model where maker = 'B'
union
select distinct Product.model, price from Printer inner join Product on Product.model = Printer.model where maker = 'B';

/*8*/
select maker from Product where type in ('PC')
except
select maker from Product where type = 'Laptop';

/*9*/
select maker from Product inner join PC on Product.model = PC.model where PC.speed >= 450 group by maker;

/*10*/
select model, price from Printer where price = (select max(price) from Printer);

/*11*/
select avg(speed) from PC;

/*12*/
select avg(speed) as AVG_SPEED from Laptop where price > 1000;

/*13*/
select avg(speed) as AVG_SPEED from PC inner join Product on Product.model = PC.model where Product.maker = 'A';

/*14*/
select Classes.class, name, Classes.country from Ships inner join Classes on Ships.class = Classes.class where Classes.numGuns >= 10;

/*15*/
select hd from PC group by hd having count(hd) >= 2;

/*16*/
select distinct PC1.model, PC.model, PC.speed, PC.ram from PC as PC1, PC where PC.speed = PC1.speed and PC.ram = PC1.ram and not PC.model = PC1.model and PC1.model > PC.model;

/*17*/
select Product.type, Laptop.model, Laptop.speed from Laptop, Product where Laptop.model = Product.model and Laptop.speed < all(select speed from PC);

/*18*/
select maker, Printer.price from Product inner join Printer on Product.model = Printer.model where Printer.price = (select min(price) from Printer where color = 'y') and Printer.color = 'y'

/*19*/
select maker, avg(Laptop.screen) from Product inner join Laptop on Product.model = Laptop.model where Product.type = 'Laptop' group by Product.maker

/*21*/
select maker, max(PC.price) from Product inner join PC on Product.model = PC.model group by maker

/*22*/
select speed, avg(price) as avg_price from PC where speed > 600 group by speed

